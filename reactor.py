import discord, asyncio
import sys, traceback, psutil, logging
import string, validators, datetime, time
from db import Database
from discord_slash import SlashCommand
from discord_slash.utils.manage_commands import create_option
from discord_slash.model import SlashCommandOptionType as OptionType
from discord.utils import escape_mentions

bootTime = int(time.time())
client = discord.Client()
slash = SlashCommand(client, sync_commands=True)
log = logging.getLogger('core')
log.setLevel(logging.INFO)
logStream = logging.StreamHandler()
logStream.setFormatter(logging.Formatter('[%(levelname)s] %(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S'))
log.addHandler(logStream)

db = Database()
teamScores = {}
challengePoints = {}
scoreboard = {}
categoryNames = {}
lastChallengeData = {}

adminChannel = db.getSetting("adminChannel")
startTime = db.getSetting("startTime")
endTime = db.getSetting("endTime")

def option(name, description, optionType = OptionType.STRING, required = True, choices = []):
	return create_option(name = name, description = description, option_type = optionType, required = required, choices = choices)

def command(name, description, options = []):
	debugGuilds = [823207698383634492]
	return slash.slash(name = name, description = description, options = options)

@client.event
async def on_ready():
	log.info(f"Fission started: {client.user.name}#{client.user.discriminator}")

	if (adminChannel != None):
		log.info(f"Admin channel set as: {adminChannel}")

@client.event
async def on_guild_channel_update(before, after):
	global categoryNames
	log.debug("Detected channel name change: %d -> %s", before.id, after.name)
	categoryNames[before.id] = after.name

@client.event
async def on_slash_command(ctx):
	if (ctx.guild_id == None):
		log.info("User %d called %s in DM", ctx.author.id, ctx.name)
	else:
		log.info("User %d called %s in guild %d", ctx.author.id, ctx.name, ctx.guild_id)

### Commands
# Public commands
@command("help", "Show help message")
async def help(ctx):
	description = """
> Commands you can use as a participant:
**/registerteam (DM Only):** Register a team with a name and password
**/jointeam (DM Only):** Join a team made by another participant who gave you the exact team name and password
**/submit (DM Only):** Submit a flag you found
**/myteam:** View info about your own team, such as members and a full matrix of solved and unsolved challenges
**/scores:** Scroll through the full scoreboard of the contest by specifying the location, or filter the scoreboard by a specific country
**/whosolved:** See a list of teams who solved a challenge
**/getteambyname:** See info about another team. Its members are not visible
**/editteam:** Edit your team's country or logo

> :information_source: **INFO:** The bot should receive and process all commands the moment you send them, but its response may be delayed by a few minutes at times. This is normal.
> :warning: **WARNING:** _This bot is in pre-pre-pre-alpha stage and has not been tested before in a real CTF. Disasters might happen._
"""

	embed = discord.Embed(title = "PLUTONIUM: Experimental Discord CTF platform", url = "https://gitlab.com/Milkdrop/plutonium", description = description)
	embed.set_thumbnail(url = "https://assets.gitlab-static.net/uploads/-/system/project/avatar/25312993/pu.png")
	embed.set_footer(text = f"PLUTONIUM ver. 0.1, rev. A\nRunning hot on a Liquid Metal Fast Breeder Reactor (LMFBR) for {int((int(time.time()) - bootTime) / 60)} minutes.")

	await ctx.send(embed = embed)

@command("registerteam", "Register your team",
	options=[option("name", "Your team's name"),
			option("password", "Your team's password"),
			option("country", "Your team's Alpha-2 country code (empty for international team)", required = False),
			option("logo", "An URL with your team's logo", required = False)])
async def registerTeam(ctx, name, password, country = "WO", logo = ""):
	if (isAfterCtfEnd()):
		return await sendNotice(ctx, "The CTF ended.")

	# Apparently the channel is None when the message is in DMs
	if (not isinstance(ctx.channel, discord.DMChannel) and ctx.channel != None):
		return await sendNotice(ctx, "Please send this command in the bot's private messages.")

	name = sanitizeTeamName(name)
	password = password.strip()

	if (len(name) == 0 or len(password) < 4):
		return await sendNotice(ctx, "Invalid team name or password too short.")

	country = country.strip().upper()[:2]
	if (not isValidCountryCode(country)):
		return await sendNotice(ctx, "Invalid country code.")

	logo = logo.strip()
	if (logo != "" and not isValidUrl(logo)):
		return await sendNotice(ctx, "Invalid team logo URL.")

	if (db.registerTeam(ctx.author.id, name, password, country, logo)):
		await ctx.send(f"Successfully created team **{name}**!")
		log.info("User %d registered team %s", ctx.author.id, name)
	else:
		await ctx.send(f"Could not create team **{name}**. Team already exists or you are already in a team.")

@command("editteam", "Edit the team you are in",
	options=[option("country", "Your team's Alpha-2 country code (empty for international team)", required = False),
			option("logo", "An URL with your team's logo", required = False)])
async def editTeam(ctx, country = "", logo = ""):
	team = db.getTeamOfUser(ctx.author.id)

	if (team == None):
		return await sendNotice(ctx, f"You are not part of any team.")

	country = country.strip().upper()[:2]
	if (not isValidCountryCode(country)):
		return await sendNotice(ctx, "Invalid country code.")

	logo = logo.strip()
	if (logo != "" and not isValidUrl(logo)):
		return await sendNotice(ctx, "Invalid team logo URL.")

	if (country == ""): country = team["country"]
	if (logo == ""): logo = team["logo"]

	await ctx.defer()
	db.editTeam(team["id"], country, logo)
	await ctx.send(f"Successfully edited team **{team['name']}**!")
	log.info("User %d edited team %s", ctx.author.id, team['name'])

@command("jointeam", "Join a team",
	options=[option("name", "Your team's name"),
			option("password", "Your team's password")])
async def joinTeam(ctx, name, password):
	if (isAfterCtfEnd()):
		return await sendNotice(ctx, "The CTF ended.")

	if (not isinstance(ctx.channel, discord.DMChannel) and ctx.channel != None):
		return await sendNotice(ctx, "Please send this command in the bot's private messages.")

	await ctx.defer()
	name = sanitizeTeamName(name)
	password = password.strip()

	if (db.joinTeam(ctx.author.id, name, password)):
		await ctx.send(f"Successfully joined team **{name}**!")
		log.info("User %d joined team %s", ctx.author.id, name)
	else:
		await ctx.send(f"Failed to join team **{name}**.")

@command("myteam", "Check your team's details")
async def myTeam(ctx):
	team = db.getTeamOfUser(ctx.author.id)

	if (team == None):
		return await sendNotice(ctx, f"You are not part of any team.")

	inAdminChannel = (ctx.channel != None and ctx.channel.id == adminChannel)
	await ctx.send(embed = await getTeamEmbed(db.getFullTeamInfo(team["id"]), showMembers = True, showChallenges = inAdminChannel))

@command("getteambyname", "Get a team by name",
	options=[option("name", "The team's name (case-sensitive substring to search)")])
async def getTeamByName(ctx, name):
	team = db.getTeamByName(name)

	if (team == None):
		return await sendNotice(ctx, f"Team not found.")

	inAdminChannel = (ctx.channel != None and ctx.channel.id == adminChannel)
	await ctx.send(embed = await getTeamEmbed(db.getFullTeamInfo(team["id"]), showMembers = inAdminChannel, showChallenges = inAdminChannel))

@command("submit", "Submit a flag",
	options=[option("flag", "The flag you found")])
async def submit(ctx, flag):
	if (not isDuringCtf()):
		return await sendNotice(ctx, "The CTF is not happening right now!")

	if (not isinstance(ctx.channel, discord.DMChannel) and ctx.channel != None):
		return await sendNotice(ctx, "Please send this command in the bot's private messages.")

	team = db.getTeamOfUser(ctx.author.id)

	if (team == None):
		return await sendNotice(ctx, f"You are not part of any team.")

	await ctx.defer()
	flag = flag.strip()
	solvedChallenges = db.createSubmission(team["id"], flag)

	if (len(solvedChallenges) == 0):
		await ctx.send(":x: Incorrect flag.")
	elif (len(solvedChallenges) == 1):
		if (solvedChallenges[0]["alreadySolved"]):
			await ctx.send(f":x: Challenge **{solvedChallenges[0]['name']}** is already solved by your team.")
		else:
			await ctx.send(f":white_check_mark: Challenge solved: **{solvedChallenges[0]['name']}**")
			log.info("User %d solved challenge %s (%d)", ctx.author.id, solvedChallenges[0]['name'], solvedChallenges[0]['id'])
	else:
		msg = "Challenges solved:\n"
		for ch in solvedChallenges:
			if (ch["alreadySolved"]):
				msg += f":x: **{ch['name']}:** Already solved by your team.\n"
			else:
				msg += f":white_check_mark: **{ch['name']}:** Challenge Solved.\n"
				log.info("User %d solved challenge %s (%d)", ctx.author.id, ch['name'], ch['id'])

		await ctx.send(msg)

@command("scores", "See 10 teams under a specific position on the scoreboard and/or from a specific country",
	options=[option("position", "topmost position of the team you want to see", OptionType.INTEGER, required = False),
			option("country", "Alpha-2 country code to filter teams by", required = False)])
async def scores(ctx, position = 1, country = None):
	if(position <= 0 or position > len(scoreboard)):
		position = 1

	if (country != None):
		country = country.strip().upper()[:2]
		if (not isValidCountryCode(country)):
			return await sendNotice(ctx, "Invalid country code.")

	description = getScoreboardMessage(position - 1, 10, country).strip()

	title = f"Top 10 teams below #{position}"
	if (country != None):
		title += " from " + getCountryEmoji(country)

	await ctx.send(embed = discord.Embed(title = title, description = description))

@command("whosolved", "See a list of teams who solved a challenge",
	options=[option("name", "The challenge's name (case-sensitive substring to search)")])
async def whosolved(ctx, name):
	if(isBeforeCtfStart()):
		return await sendNotice(ctx, "The CTF did not start yet!")

	challenge = db.getChallengeByName(name)

	if (challenge == None):
		return await sendNotice(ctx, f"No challenge found with a name similar to **{name}**")

	await ctx.defer()

	description = ""
	solvers = db.getSolvesForChallenge(challenge["id"])
	if (len(solvers) == 0):
		description = "No teams solved this challenge."
	else:
		description = ", ".join([team["name"] for team in solvers])

		if (len(description) > 2048):
			description = description[:2045]
			description = description[:description.rfind(",") - 1] + "..."

	await ctx.send(embed = discord.Embed(title = f"Teams who solved: **{challenge['name']}**", description = description))

# Private Commands
@command("setadminchannel", "(Admin) Set the channel on which to listen for all admin commands",
	options=[option("textChannel", "Text Channel", OptionType.CHANNEL)])
async def setAdminChannel(ctx, channel):
	global adminChannel

	if (adminChannel != None):
		return await sendNotice(ctx)

	if (not isinstance(channel, discord.TextChannel)):
		return await sendNotice(ctx, f"<#{channel.id}> is not a text channel!")
	
	await ctx.defer()
	db.setSetting("adminChannel", channel.id)
	adminChannel = channel.id

	await ctx.send(f"Admin channel set as <#{channel.id}>")
	log.info("Admin channel set as %d", channel.id)

@command("setscoreboardchannel", "(Admin) Set the channel on which to post the scoreboard",
	options=[option("textChannel", "Text Channel", OptionType.CHANNEL)])
async def setScoreboardChannel(ctx, channel):
	if (ctx.channel == None or ctx.channel.id != adminChannel):
		return await sendNotice(ctx)

	if (not isinstance(channel, discord.TextChannel)):
		return await sendNotice(ctx, f"<#{channel.id}> is not a text channel!")

	await ctx.defer()
	db.setSetting("scoreboardChannel", channel.id)

	await ctx.send(f"Scoreboard channel set as <#{channel.id}>")
	log.info("Scoreboard channel set as %d", channel.id)

@command("setctftimes", "(Admin) Set when the CTF starts and ends (this blocks new submissions and teams)",
	options=[option("starttime", "UNIX Timestamp of the start date (UTC+0)", OptionType.INTEGER),
			option("endtime", "UNIX Timestamp of the end date (UTC+0)", OptionType.INTEGER)])
async def setCtfTimes(ctx, starttime, endtime):
	global startTime
	global endTime
	
	if (ctx.channel == None or ctx.channel.id != adminChannel):
		return await sendNotice(ctx)

	if (starttime < 0 or endtime < 0 or starttime > endtime):
		return await sendNotice(ctx, "Invalid times.")

	await ctx.defer()

	# yuck!
	startTime = starttime
	endTime = endtime
	db.setSetting("startTime", starttime)
	db.setSetting("endTime", endtime)

	await ctx.send(f"CTF dates are now set:\n`Begins on:` **{getPrettyTime(starttime)}**\n`  Ends on:` **{getPrettyTime(endtime)}**")
	log.info("CTF times set as %d - %d", starttime, endtime)

@command("createChallenge", "(Admin) Create a new challenge",
	options=[option("name", "Name of the challenge"),
			option("category", "Category of the challenge", OptionType.CHANNEL),
			option("flag", "Flag of the challenge"),
			option("description", "Description of the challenge", required = False),
			option("target", "URL of the challenge server to attack", required = False),
			option("files", "URL for downloading the challenge's files", required = False),
			option("initpoints", "Initial points for the challenge (default: 500)", OptionType.INTEGER, required = False),
			option("minpoints", "Minimum points for the challenge (default: 50)", OptionType.INTEGER, required = False),
			option("solvedecay", "Number of solves required to reach minimum points (default: 100)", OptionType.INTEGER, required = False)])
async def createChallenge(ctx, name, category, flag, description = "", target = "", files = "", initpoints = 500, minpoints = 50, solvedecay = 100):
	if (ctx.channel == None or ctx.channel.id != adminChannel):
		return await sendNotice(ctx)

	if (not isinstance(category, discord.TextChannel)):
		return await sendNotice(ctx, f"<#{category.id}> is not a text channel!")

	await ctx.defer()
	title = f"{name} - [{initpoints} Points] [0 Solves]"
	challenge = discord.Embed(title = title, description = processChallengeDescription(description, target, files), color = 0xFF8D23)
	ch = await category.send(embed = challenge)

	db.createChallenge(ch.id, name, category.guild.id, category.id, flag.strip(), description, target, files, initpoints, minpoints, solvedecay)
	await ctx.send(f"Challenge **{name}** created.")
	log.info("Challenge %s (%d) created", name, ch.id)

@command("editchallenge", "(Admin) Edit a challenge",
	options=[option("challengeid", "ID of the challenge message with the embed. eg: 824297981137387581"),
			option("name", "Name of the challenge", required = False),
			option("flag", "Flag of the challenge", required = False),
			option("description", "Description of the challenge", required = False),
			option("target", "URL of the challenge server to attack", required = False),
			option("files", "URL for downloading the challenge's files", required = False),
			option("initpoints", "Initial points for the challenge (default: 500)", OptionType.INTEGER, required = False),
			option("minpoints", "Minimum points for the challenge (default: 50)", OptionType.INTEGER, required = False),
			option("solvedecay", "Number of solves required to reach minimum points (default: 100)", OptionType.INTEGER, required = False)])
async def editChallenge(ctx, challengeid, name = None, flag = None, description = None, target = None, files = None, initpoints = None, minpoints = None, solvedecay = None):
	if (ctx.channel == None or ctx.channel.id != adminChannel):
		return await sendNotice(ctx)

	challengeData = db.getChallenge(challengeid)
	if (challengeData == None):
		return await sendNotice(ctx, f"No challenge was found with ID: **{challengeid}**")

	await ctx.defer()
	if (name == None): name = challengeData["name"]
	if (flag == None): flag = challengeData["flag"]
	if (description == None): description = challengeData["description"]
	if (target == None): target = challengeData["target"]
	if (files == None): files = challengeData["files"]
	if (initpoints == None): initpoints = challengeData["initpoints"]
	if (minpoints == None): minpoints = challengeData["minpoints"]
	if (solvedecay == None): solvedecay = challengeData["solvedecay"]

	points = dynamicScoringFormula(initpoints, minpoints, solvedecay, challengeData["solves"])
	title = f"{name} - [{points} Points] [{challengeData['solves']} Solves]"
	challenge = discord.Embed(title = title, description = processChallengeDescription(description, target, files), color = 0xFF8D23)

	cat = await client.fetch_channel(challengeData["category"])
	msg = await cat.fetch_message(challengeid)

	if (msg.embeds[0].footer != discord.Embed.Empty):
		challenge.set_footer(text = msg.embeds[0].footer.text)

	await msg.edit(embed = challenge)

	db.editChallenge(challengeid, name, cat.guild.id, challengeData["category"], flag.strip(), description, target, files, initpoints, minpoints, solvedecay)
	await ctx.send(f"Challenge **{name}** edited.")
	log.info("Challenge %s (%d) edited", name, int(challengeid))

@command("removechallenge", "(Admin) Remove a challenge",
	options=[option("challengeid", "ID of the challenge message with the embed. eg: 824297981137387581")])
async def removeChallenge(ctx, challengeid):
	if (ctx.channel == None or ctx.channel.id != adminChannel):
		return await sendNotice(ctx)

	challengeData = db.getChallenge(challengeid)
	if (challengeData == None):
		return await sendNotice(ctx, f"No challenge was found with ID: **{challengeid}**")

	await ctx.defer()
	cat = await client.fetch_channel(challengeData["category"])
	msg = await cat.fetch_message(challengeid)
	await msg.delete()

	db.removeChallenge(challengeid)
	await ctx.send(f"Challenge **{challengeData['name']}** removed.")
	log.info("Challenge %s (%d) removed", challengeData['name'], int(challengeid))

@command("getteam", "(Admin) Get an user's team",
	options=[option("user", "The user", OptionType.USER)])
async def getTeam(ctx, user):
	if (ctx.channel == None or ctx.channel.id != adminChannel):
		return await sendNotice(ctx)

	team = db.getTeamOfUser(user.id)

	if (team == None):
		return await sendNotice(ctx, f"User is not part of any team.")

	await ctx.send(embed = await getTeamEmbed(db.getFullTeamInfo(team["id"]), showMembers = True, showChallenges = True))

@command("addpoints", "(Admin) Add or remove an arbitrary number of points from an user's team",
	options=[option("user", "The user", OptionType.USER),
			option("points", "The number of points", OptionType.INTEGER)])
async def addpoints(ctx, user, points):
	if (ctx.channel == None or ctx.channel.id != adminChannel):
		return await sendNotice(ctx)

	team = db.getTeamOfUser(user.id)

	if (team == None):
		return await sendNotice(ctx, f"User is not part of any team.")

	await ctx.defer()
	db.addPoints(team["id"], points)
	await ctx.send(f"Team **{team['name']}** has been awarded **{points} Points!**")

@command("stats", "(Admin) Get information about the CTF",
	options=[option("minutes", "Show how many solves in the past X minutes", OptionType.INTEGER, required = False)])
async def stats(ctx, minutes = 5):
	if (ctx.channel == None or ctx.channel.id != adminChannel):
		return await sendNotice(ctx)

	await ctx.defer()
	if (startTime == None or endTime == None):
		description = ":warning: CTF Dates are not set. Users will not be able to register or submit any flags.\n"
	else:
		description = f"CTF Dates: **{getPrettyTime(startTime)} - {getPrettyTime(endTime)}**\n"

	description += f"Total: **{db.getUsersCount()}** users | **{db.getTeamsCount()}** teams | **{db.getSolvesCount()}** solves\n"
	description += f"Challenges solved: **{db.getSolvedChallengesCount()}/{db.getChallengesCount()}**\n\n"
	description += f"Solves in the past {minutes} minutes: **{db.getSolvesPastTimestamp(int(time.time()) - minutes * 60)}**\n\n"

	description += f"CPU Usage: **{round (psutil.cpu_percent (), 2)}%**\n"
	mem = psutil.virtual_memory ()
	description += f"Memory usage: **{round(mem.used / (1024 ** 2))}/{round(mem.total / (1024 ** 2))} MB**"

	embed = discord.Embed(title = "CTF Stats", description = description)

	await ctx.send(embed = embed)

### Routines
async def commitChangesToDatabase():
	while True:
		db.commitToDatabase()
		await asyncio.sleep(60)

async def updateChallengePoints():
	await client.wait_until_ready()

	while True:
		for challenge in db.getAllChallenges():
			points = dynamicScoringFormula(challenge["initpoints"], challenge["minpoints"], challenge["solvedecay"], challenge["solves"])
			msg = None
			embed = None

			try:
				if (challenge["id"] not in lastChallengeData):
					cat = client.get_channel(challenge["category"])
					if (cat == None): cat = await client.fetch_channel(challenge["category"])

					msg = await cat.fetch_message(challenge["id"])
					embed = msg.embeds[0]

					name = embed.title
					lastChallengeData[challenge["id"]] = {}
					lastChallengeData[challenge["id"]]["points"] = int(name[name.rfind(" - [") + 4:name.rfind(" Points")])
					lastChallengeData[challenge["id"]]["solves"] = int(name[name.rfind("] [") + 3:name.rfind(" Solves")])

				if (lastChallengeData[challenge["id"]]["solves"] != challenge["solves"] or lastChallengeData[challenge["id"]]["points"] != points):
					if (msg == None or embed == None):
						cat = client.get_channel(challenge["category"])
						if (cat == None): cat = await client.fetch_channel(challenge["category"])

						msg = await cat.fetch_message(challenge["id"])
						embed = msg.embeds[0]

					lastChallengeData[challenge["id"]]["points"] = points
					lastChallengeData[challenge["id"]]["solves"] = challenge["solves"]
					name = embed.title
					embed.title = name[:name.rfind(" - [")] + f" - [{points} Points] [{challenge['solves']} Solves]"

					topSolvers = db.getSolvesForChallenge(challenge["id"])[:10]

					embed.set_footer(text = "")
					if (len(topSolvers) != 0):
						embed.set_footer(text = "Top 10 solvers: " + ", ".join([f"{team['name']}" for team in topSolvers]))

					await msg.edit(embed = embed)
					log.info("Challenge %s (%d) got updated. Points: %d, Solves: %d", challenge["name"], challenge["id"], points, challenge["solves"])
					await asyncio.sleep(5)

			except Exception as e:
				print("Exception in updateChallengePoints")
				print(e)

		await asyncio.sleep(5)

async def updateScoreboard():
	global teamScores
	global challengePoints
	global scoreboard

	await client.wait_until_ready()

	while True:
		challengePoints = {} # id -> points
		for challenge in db.getAllChallenges():
			challengePoints[challenge["id"]] = dynamicScoringFormula(challenge["initpoints"], challenge["minpoints"], challenge["solvedecay"], challenge["solves"])

		scores = {}
		teamData = {}
		for teamSolve in db.getAllSolves():
			if (teamSolve["team"] not in scores):
				teamData[teamSolve["team"]] = teamSolve
				scores[teamSolve["team"]] = challengePoints[teamSolve["challenge"]] + teamSolve["bonuspoints"]
			else:
				scores[teamSolve["team"]] += challengePoints[teamSolve["challenge"]]

		for team in db.getTeamsWithoutSolves():
			teamData[team["id"]] = team
			scores[team["id"]] = team["bonuspoints"]

		scores = sorted(scores.items(), key=__import__("operator").itemgetter(1), reverse=True)
		scoreboard = {}
		teamScores = {}

		for i in range(len(scores)):
			teamId = scores[i][0]
			teamScores[teamId] = {"points": scores[i][1], "position": i + 1}
			scoreboard[i] = {"name": teamData[teamId]["name"], "country": teamData[teamId]["country"], "points": scores[i][1]}

		scoreboardChannel = db.getSetting("scoreboardChannel")

		if (scoreboardChannel != None):
			channel = client.get_channel(scoreboardChannel)
			if (channel == None):
				try:
					channel = await client.fetch_channel(scoreboardChannel)
				except:
					db.setSetting("scoreboardChannel", None)
					log.warning("Could no longer find scoreboard channel")
					continue

			scoreboardEmbed = discord.Embed(title = f"**Scoreboard - {len(teamScores)} total teams**", description = getScoreboardMessage(0, 20).strip())
			scoreboardMessage = db.getSetting("scoreboardMessage")

			if (scoreboardMessage == None):
				msg = await channel.send(embed = scoreboardEmbed)
				log.info("Created scoreboard message: %d", msg.id)
				db.setSetting("scoreboardMessage", msg.id)
			else:
				try:
					msg = await channel.fetch_message(scoreboardMessage)
				except: # Message got deleted
					msg = await channel.send(embed = scoreboardEmbed)
					db.setSetting("scoreboardMessage", msg.id)
					log.warning("Could not fetch old scoreboard message (%d)? Creating a new message: %d", scoreboardMessage, msg.id)

				if (msg.embeds[0].title != scoreboardEmbed.title or msg.embeds[0].description != scoreboardEmbed.description):
					await msg.edit(embed = scoreboardEmbed)
					log.info("Updated scoreboard.")
		else:
			log.warning("Scoreboard channel is not set.")

		await asyncio.sleep(15)

def getScoreboardMessage(topmostPosition, numberOfTeams, country = None):
	message = ""

	added = 0
	for i in range(topmostPosition, len(scoreboard)):
		if (added >= numberOfTeams):
			break

		if (country == None or country.upper() == scoreboard[i]["country"].upper()):
			message += f"#{i + 1}. **{scoreboard[i]['name']}** ({getCountryEmoji(scoreboard[i]['country'])}): {scoreboard[i]['points']} Points\n"
			added += 1

	if (len(message) == 0):
		message = "No teams."

	return message

### Utils
def processChallengeDescription(description, target, files):
	description = description.replace("\\n", "\n")

	if (target != ""):
		if (isValidUrl(target)):
			description += f"\n\n**Target:** [{target}]({target})\n"
		else:
			description += f"\n\n**Target:** {target}\n"

	if (files != ""):
		if (target == ""): description += "\n\n"
		if (isValidUrl(files)):
			description += f"**Challenge Files:** [{files}]({files})\n"
		else:
			description += f"**Challenge Files:** {files}\n"

	return description

def dynamicScoringFormula(initpoints, minpoints, solvedecay, solves):
	if (solvedecay == 0):
		return initpoints
	elif (solves >= solvedecay):
		return minpoints
	else:
		return initpoints - int((initpoints - minpoints) * (solves ** 2) / (solvedecay ** 2))

def getCountryEmoji(countryCode):
	if (countryCode == "WO"):
		return '\U0001F310'
	else:
		return ''.join([chr(ord(c)+127397) for c in countryCode]) # Nifty conversion from Alpha-2 to unicode!

def sanitizeTeamName(name):
	return escape_mentions(name.strip())[:50].replace('*', '\\*').replace('_', '\\_').replace('`', '\\`').replace('|', '\\|').strip()

def isValidUrl(url):
	return (url.startswith("http://") or url.startswith("https://")) and validators.url(url)

def isValidCountryCode(countryCode):
	if (len(countryCode) != 2):
		return False

	for c in countryCode:
		if (c not in string.ascii_uppercase):
			return False

	return True

def isBeforeCtfStart():
	if (startTime == None): return True
	currentTime = int(time.time())
	return currentTime < startTime

def isAfterCtfEnd():
	if (endTime == None): return False
	currentTime = int(time.time())
	return currentTime >= endTime

def isDuringCtf():
	if (startTime == None or endTime == None): return False
	currentTime = int(time.time())
	return currentTime >= startTime and currentTime < endTime

def getPrettyTime(timestamp):
	return datetime.datetime.utcfromtimestamp(timestamp).strftime('%Y-%m-%d %H:%M:%S UTC')

# Ugh, needs async
async def getTeamEmbed(team, showMembers = True, showChallenges = False):
	title = f"**{team['name']}** ({getCountryEmoji(team['country'])})"

	if (team['id'] in teamScores):
		position = teamScores[team['id']]['position']
		title = f"#{position}. {title}: {teamScores[team['id']]['points']} Points"

		bonus = team["bonuspoints"]
		if (bonus != 0):
			if (bonus < 0): title += f" ({bonus}pts Penalty)"
			else: title += f" ({+bonus}pts Bonus)"

		if (position == 1): title += " :first_place:"
		if (position == 2): title += " :second_place:"
		if (position == 3): title += " :third_place:"
	else:
		title += ": ? Points"

	if (showMembers == True):
		if (len(team["members"]) == 0):
			description = "**No members.**\n"
		else:
			description = "**Members:**\n"

			for i in range(len(team["members"])):
				description += f"{i + 1}. <@{team['members'][i]['id']}>\n"
	else:
		description = "**Members hidden.**\n"

	totalLength = len(title) + len(description)
	teamEmbed = discord.Embed(title = title, description = description, color = 0x23B2FF)

	if(not isBeforeCtfStart() or showChallenges):
		categories = {}
		for challenge in team["challenges"]:
			cat = challenge["category"]
			if (cat not in categoryNames):
				categoryNames[cat] = (await client.fetch_channel(cat)).name

			if (categoryNames[cat] not in categories): categories[categoryNames[cat]] = [challenge]
			else: categories[categoryNames[cat]].append(challenge)

		for cat in categories:
			catMessage = getCategoryMessage(categories[cat])
			if (len(catMessage) > 1024):
				catMessage = getCategoryMessage(categories[cat], False)

			teamEmbed.add_field(name = f"#{cat}:", value = catMessage, inline = True)
			totalLength += len(f"#{cat}:") + len(catMessage)

		if (totalLength >= 5900):
			teamEmbed.clear_fields()

			for cat in categories:
				catMessage = getCategoryMessage(categories[cat], False)
				teamEmbed.add_field(name = f"#{cat}:", value = catMessage, inline = True)
				totalLength += len(f"#{cat}:") + len(catMessage)

		# Add empty category for pretty alignment
		if (len(categories) % 3 == 2):
			teamEmbed.add_field(name = "\u200b", value = "\u200b", inline = True)

	if(team["logo"] != ""):
		teamEmbed.set_thumbnail(url = team["logo"])

	return teamEmbed

def getCategoryMessage(categoryData, showLinks = True):
	if (len(categoryData) == 0): catMessage = "\u200b"
	else: catMessage = ""

	for challenge in categoryData:
		pointsText = ""

		if (challenge["solved"] == 1):
			catMessage += ":white_check_mark: "
			pointsText = "(?pts)"
			if (challenge["id"] in challengePoints):
				pointsText = f"({challengePoints[challenge['id']]}pts)"
		else: catMessage += ":x: "

		if (showLinks):
			catMessage += f"[{challenge['name']}](https://discord.com/channels/{challenge['guild']}/{challenge['category']}/{challenge['id']}) {pointsText}\n"
		else:
			catMessage += f"{challenge['name']} {pointsText}\n"

	return catMessage

async def sendNotice(ctx, notice="Unauthorized.", noticeType="error"):
	await ctx.send(f":x: **{noticeType.upper()}:** {notice}", hidden = True)

client.loop.create_task(commitChangesToDatabase())
client.loop.create_task(updateChallengePoints())
client.loop.create_task(updateScoreboard())
client.run(open("token").read().strip())