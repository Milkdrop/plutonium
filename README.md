# Plutonium

A highly-experimental, highly-volatile, extremely lightweight CTF platform that runs entirely on Discord.

### What is this?
This is a bot that hosts an entire CTF competition within itself. It's designed to be easy to use by participants and much less of a hassle to host compared to a conventional CTF platform.

### How does this Plutonium mumbo-jumbo work?
- **Teams:** Teams have a name, password, country code and image logo. They are made up of any number of Discord users.
- **Challenges:** Each challenge is a message with an embed posted on a Discord channel (e.g. #web-exploitation). Every challenge has dynamic scoring, and the number of points listed in the embed auto-updates at a regular interval.
- **Flag submission:** Whenever an user finds a flag, they can submit it via the **/submit** command in the bot's DMs. The bot is configured to talk to the Discord API in order to completely block any **/submit** commands done publicly, in order to prevent flag leaks.
- **Scoreboard:** A single Discord channel (e.g. #scoreboard) could be used by the bot to post and regularly update an embed with the top 20 teams. Users can also use a **/scores** command to manually scroll through the whole scoreboard.
- **Administration:** The bot listens for admin commands on a single Discord channel (e.g. #admin). An admin command sent on any other channel will be unauthorized.

### Why would I use this instead of CTF(d|x)?
- Plutonium saves you the hassle of setting up a conventional server, installing and configuring the full website software stack like nginx, python/php-fpm, a mysql database server, etc... This bot can be spun up anywhere with python3, and the whole contest is stored in a simple SQLite3 `data.db` file, meaning that you can easily carry the whole CTF around and host it wherever with ease. It's very portable and easy to set up.
- Plutonium also saves you the stress of having to deal with lots of security headaches, since the attack surface is much smaller than the one of an usual website. You don't really have to worry about random 0days, people DDoSing the website, or convoluted SQLi / XSS setups that come from complex CTF platfom code.

### How do I set this thing up?
- `pip3 install discord.py discord-py-slash-command validators psutil`
- Have a "token" file in your path with your Discord bot token
- Run `python3 reactor.py` to start the bot
- **/setadminchannel**: Set the CTF admin channel on which you will send further admin commands to the bot (if you delete this channel you will have to manually reset the "adminChannel" variable in the SQLite3 database).
- **/setctftimes**: Set when the CTF starts and ends.
- **/setscoreboardchannel**: Set a scoreboard channel on which the bot will post the CTF scoreboard.
- You can now create your challenges by using the **/createchallenge** command! Make sure you hide the Discord channels on which these challenges are posted if the CTF has not started yet, as users can see the challenges regardless of the competition's start and end times.
- There are plenty of other admin commands you can use, you can see them inside Discord when you do **/** in the message box.
- Happy CTF organizing!

### Extra info :warning:
- The bot needs an "application.commands" permission in the guild it's invited to. An example invite link would be: https://discord.com/api/oauth2/authorize?client_id=[YOUR_ID_HERE]&permissions=0&scope=bot%20applications.commands
- An **important** thing to note is that the bot may not respond immediately after receiving a command. The bot should receive \
and process all commands when you send them, but the response might be delayed even by a few minutes at times. This is normal.
- This bot is in pre-pre-pre-alpha stage. Disasters might happen. While the codebase is small and everything seems rather stable, \
it has not been tested before. This is the first time we are using Plutonium to run a CTF.
