import sqlite3, hashlib

class Database():
	def __init__(self):
		self.conn = sqlite3.connect('data.db')
		self.conn.row_factory = sqlite3.Row
		self.cur = self.conn.cursor()

		tables = [table[0] for table in self.cur.execute("SELECT name FROM sqlite_master WHERE type='table'").fetchall()]

		if ("settings" not in tables):
			self.cur.execute("CREATE TABLE settings(name text, value text)")

		if ("challenges" not in tables):
			self.cur.execute("CREATE TABLE challenges(id int PRIMARY KEY, name text, guild int, category int, flag text, description text, target text, files text, initpoints int, minpoints int, solvedecay int)")

		if ("teams" not in tables):
			self.cur.execute("CREATE TABLE teams(id INTEGER PRIMARY KEY AUTOINCREMENT, name text UNIQUE, password text, country text, logo text, bonuspoints int default 0)")

		if ("users" not in tables):
			self.cur.execute("CREATE TABLE users(id int PRIMARY KEY, team int)")

		if ("solves" not in tables):
			self.cur.execute("CREATE TABLE solves(challenge int, team int, time timestamp default (strftime('%s', 'now')), PRIMARY KEY(challenge, team))")

		self.cur.execute("PRAGMA case_sensitive_like = true;")
		self.conn.commit()

	def getSetting(self, name):
		row = self.cur.execute("SELECT value FROM settings WHERE name=?", (name,)).fetchone()
		if (row == None): return None
		else:
			if (row["value"].isdigit()): return int(row["value"])
			else: return row["value"]

	def getChallenge(self, challengeId):
		return self.cur.execute("SELECT *, (SELECT COUNT(*) FROM solves WHERE challenge=id) AS solves FROM challenges WHERE id=?", (challengeId,)).fetchone()

	def getChallengeByName(self, name):
		return self.cur.execute("SELECT * FROM challenges WHERE name LIKE '%' || ? || '%'", (name,)).fetchone()

	def getSolvesForChallenge(self, challengeId):
		return self.cur.execute("SELECT name, solves.time FROM teams JOIN solves ON solves.team=teams.id AND solves.challenge=? ORDER BY solves.time", (challengeId,)).fetchall()

	def getAllChallenges(self):
		return self.cur.execute("SELECT category, id, name, initpoints, minpoints, solvedecay, (SELECT COUNT(*) FROM solves WHERE challenge=id) AS solves FROM challenges").fetchall()

	def getAllSolves(self):
		return self.cur.execute("SELECT * FROM solves JOIN teams ON id=team").fetchall()

	def getTeamsWithoutSolves(self):
		return self.cur.execute("SELECT * FROM teams WHERE id NOT in (SELECT team FROM solves)").fetchall()

	# User actions
	def registerTeam(self, userId, name, password, country, logo):
		if (self.cur.execute("SELECT * FROM users WHERE id=?", (userId,)).fetchone() != None):
			return False

		try:
			self.cur.execute("INSERT INTO teams(name, password, country, logo) VALUES (?, ?, ?, ?)", (name, hashlib.sha256((name + "//" + password).encode()).hexdigest(), country, logo))
		except sqlite3.IntegrityError:
			return False

		teamId = self.cur.execute("SELECT last_insert_rowid() AS id").fetchone()["id"]
		self.assignUserToTeam(userId, teamId)
		return True

	def editTeam(self, teamId, country, logo):
		self.cur.execute("UPDATE teams SET country=? WHERE id=?", (country, teamId))
		self.cur.execute("UPDATE teams SET logo=? WHERE id=?", (logo, teamId))

	def joinTeam(self, userId, name, password):
		team = self.cur.execute("SELECT id FROM teams WHERE name=? AND password=?", (name, hashlib.sha256((name + "//" + password).encode()).hexdigest())).fetchone()
		if (team == None): return False

		self.assignUserToTeam(userId, team["id"])
		return True

	def getTeamOfUser(self, userId):
		team = self.cur.execute("SELECT * FROM teams WHERE id=(SELECT team FROM users WHERE id=?)", (userId,)).fetchone()
		if (team == None): return None
		else: return team

	def getFullTeamInfo(self, teamId):
		team = dict(self.cur.execute("SELECT * FROM teams WHERE id=?", (teamId,)).fetchone())
		team["members"] = self.cur.execute("SELECT id FROM users WHERE team=?", (team["id"],)).fetchall()
		team["challenges"] = self.cur.execute("SELECT id, name, guild, category, (id IN (SELECT challenge FROM solves WHERE team=?)) as solved FROM challenges", (team["id"],)).fetchall()

		return team

	def assignUserToTeam(self, userId, teamId):
		self.cur.execute("INSERT INTO users VALUES(?, ?) ON CONFLICT(id) DO UPDATE SET team=excluded.team;", (userId, teamId))

	def createSubmission(self, team, flag):
		solvedChallenges = self.cur.execute("SELECT id, name FROM challenges WHERE flag=?", (flag, )).fetchall()

		for i in range(len(solvedChallenges)):
			solvedChallenges[i] = dict(solvedChallenges[i])

			try:
				self.cur.execute("INSERT INTO solves(challenge, team) VALUES(?, ?)", (solvedChallenges[i]["id"], team))
				solvedChallenges[i]["alreadySolved"] = False
			except sqlite3.IntegrityError:
				solvedChallenges[i]["alreadySolved"] = True

		return solvedChallenges

	# Admin actions
	def setSetting(self, name, value):
		self.cur.execute("DELETE FROM settings WHERE name=?", (name,))
		if (value != None):
			self.cur.execute("INSERT INTO settings VALUES (?, ?)", (name, value))
			
		self.conn.commit()

	def getTeamByName(self, name):
		team = self.cur.execute("SELECT * FROM teams WHERE name LIKE '%' || ? || '%'", (name,)).fetchone()
		if (team == None): return None
		else: return team

	def createChallenge(self, *args):
		self.cur.execute("INSERT INTO challenges VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", args)
		self.conn.commit()

	def addPoints(self, teamId, points):
		self.cur.execute("UPDATE teams SET bonuspoints=bonuspoints+? WHERE id=?", (points, teamId))
		self.conn.commit()

	# Stats actions
	def getTeamsCount(self):
		return self.cur.execute("SELECT COUNT(*) FROM teams").fetchone()[0]

	def getUsersCount(self):
		return self.cur.execute("SELECT COUNT(*) FROM users").fetchone()[0]

	def getSolvesCount(self):
		return self.cur.execute("SELECT COUNT(*) FROM solves").fetchone()[0]

	def getChallengesCount(self):
		return self.cur.execute("SELECT COUNT(*) FROM challenges").fetchone()[0]

	def getSolvedChallengesCount(self):
		return self.cur.execute("SELECT COUNT(*) FROM challenges WHERE id IN (SELECT challenge FROM solves)").fetchone()[0]

	def getSolvesPastTimestamp(self, timestamp):
		return self.cur.execute("SELECT COUNT(*) FROM solves WHERE time > ?", (timestamp,)).fetchone()[0]

	# f"UPDATE challenges SET {' '.join(['?=?' for i in a.keys()])} WHERE id=?", sum(kwargs.items(), ()) + (challengeid,))
	def editChallenge(self, *args):
		self.cur.execute("DELETE FROM challenges WHERE id=?", (args[0], ))
		self.cur.execute("INSERT INTO challenges VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", args)
		self.conn.commit()

	def removeChallenge(self, challengeId):
		self.cur.execute("DELETE FROM challenges WHERE id=?", (challengeId, ))
		self.cur.execute("DELETE FROM solves WHERE challenge=?", (challengeId, ))
		self.conn.commit()

	def commitToDatabase(self):
		self.conn.commit()